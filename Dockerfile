# syntax=docker/dockerfile:1
FROM python:alpine AS base

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
RUN pip install soccerpy