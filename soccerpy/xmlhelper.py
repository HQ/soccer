##########################################################################
#
#       hangxia qiu
#
##########################################################################

import os, sys, subprocess, platform
import lxml.etree as le
import lxml.objectify as objectify
from operator import attrgetter
from copy import deepcopy

#
# Prepares the location of the temporary file that will be created by xmldiff
def createFileObj(prefix, name):
    return {
        "filename" : os.path.abspath(name),
        "tmpfilename" : "." + prefix + "." + os.path.basename(name)
    }

#
# Function to get list of files from a provided path
# No sub folders in path
def getFiles(path):
        if not os.path.isdir(path):
                return 'Invalid path'
        onlyfiles = [ f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) ]
        #for file in onlyfiles:
        #       print path + '/' + file + ' is a file'
        return onlyfiles

def createElement(xmlstring):
	return objectify.fromstring(xmlstring)

def getElement(xmldoc, xpath_string):
	return xmldoc.xpath(xpath_string)
	
#
# This function removes elements based on clean_list and return a new element list
def cleanElement(elements, clean_list):
	new_elements = deepcopy(elements) 
	num_removed = 0
	for e in elements:
		for c in clean_list:
			k = list(c.keys())[0]
			if k in e.attrib.keys() and c[k] == e.attrib[k]:
				#print("Removing " + le.tostring(e, pretty_print=True))
				new_elements.pop(elements.index(e)- num_removed)
				num_removed = num_removed + 1 

	return new_elements
			
def appendElement(location, elements):
	for e in elements:
		location.append(e)

def addAttribute(xmldoc, key, value):
	xmldoc.attrib[key] = value
	return xmldoc

def parseXMLFile(fileobj):
    with open(fileobj['filename'], 'r') as original:
        # parse the XML file and get a pointer to the top
        parser = le.XMLParser(remove_blank_text=True)
        xmldoc = le.parse(original, parser)
        xmlroot = xmldoc.getroot()
        #print(le.tostring(xmldoc, pretty_print=True))

    return xmldoc

def generateDictionary(path, file_list, xml_path, attr):
	dictionary = {}
	for f in file_list:
		fobj = createFileObj("temp", path + '/' + f)
		fxml = parseXMLFile(fobj)
		key = getElement(fxml, xml_path)
		dictionary[key[0].attrib[attr]] = f 
	return dictionary

#
# Compare two XML element based on an attribute
# Example: compareElement(MSVAS1[0], MSVAS2[0], 'Value')
def compareElement(element1, element2, attr):
	return element1.attrib[attr] == element2.attrib[attr]

#
# Find parent and append there
def mergeXMLElement(xmldoc1, xmldoc2, section):
	list1 = getElement(xmldoc1, section)
	parent1 = list1[0].getparent()
	parent1.append(xmldoc2)

	#for ce in parent1:
		#print(le.tostring(ce, pretty_print=True))

#
# Merge xmldoc2 into xmldoc2 based on xid, section to merge
def mergeXML(xmldoc1, xmldoc2, section, clean_attr_list):
	list1 = getElement(xmldoc1, section)
	parent1 = list1[0].getparent()

	list2 = getElement(xmldoc2, section)
	parent2 = list2[0].getparent()
	elements2 = parent2.getchildren();

	cleaned_elements = cleanElement(elements2, clean_attr_list)
	appendElement(parent1, cleaned_elements)	

	#for ce in parent1:
		#print(le.tostring(ce, pretty_print=True))
	return xmldoc1
