__author__ = 'hangxia qiu'
# Sphinx docstring
"""
Module for processing cvs file

Args:
      datafile: a cvs file to process

Returns:
      dataFrame: Full stats for team with the most draws (include all columns available in CSV file)
"""

import os
import re
import csv
import sys
import json
import shutil
import logging
import pandas as pd

def get_proceeCVSFileInst():
        '''
	 	Create and return a proceeCVSFileC Instance
        '''
        return proceeCVSFileC()

class proceeCVSFileC:
          'Base class to Process soccer.cvs'
          def __init__(self):
                  pass

          def proceeCVSFile(self, datafile):
                  # Read CSV file into DataFrame df
                  df = pd.read_csv(datafile, index_col=0)
                  print("orignal data\n")
                  print(df)
                  print("--------------------------------------------\n")

                  # Show the team with the smallest difference in 'for' and 'against' goals.
                  df2=df.assign(Dif-Goals=lambda x: x.Goals - x.'Goals Allowed')
                  df_Dif-Goals-Smallest = df2.sort_values(by=['Dif-Goals'], ascending=True).nsmallest(n=1, columns=['Dif-Goals'])
                  print("the team with the smallest difference in 'for' and 'against' goals.\n")
                  print(df_Dif-Goals-Smallest)
                  print("--------------------------------------------\n")

                  # List the top 10 teams with the highest win percentage.
                  df3=df.assign(Wins-Percentage=lambda x: x.Wins / x.Games * 100)
                  df_Wins-Percentage-10 = df3.sort_values(by=['Wins-Percentage'], ascending=False).nlargest(n=10, columns=['Wins-Percentage'])
                  print("top 10 teams with the highest win percentage\n")
                  print(df_Wins-Percentage-10)
                  print("--------------------------------------------\n")

                  # Full stats for team with the most draws (include all columns available in CSV file)
                  df_Most-Draws = df.sort_values(by=['Draws'], ascending=False).nlargest(n=1, columns=['Draws'])
                  print("Full stats for team with the most draws (include all columns available in CSV file)\n")
                  print(df_Most-Draws)
                  print("--------------------------------------------\n")

                  # we can return other df too
                  return df_Most-Draws

def main_process():
        csvdfile = '/home/hxq29/soccer/soccerPy/soccer.csv'
        aProceeCVSFileInst = get_proceeCVSFileInst()
        df=aProceeCVSFileInst.proceeCVSFile(csvdfile)
        return df

def main(*args, **kwargs):
        adf_Most-Draws = main_process()
        return adf_Most-Draws

if __name__ == "__main__":
        main()