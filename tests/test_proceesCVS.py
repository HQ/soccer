####################################                 
# Test:                                                                                      # fixture:      the data that you create as an input 
# a. Install nose2 coverage pyhamcrest sphinx pylint pytest tox                              # side effects: executing a piece of code will alter other things in the environment
# b. Pylint: pycodestyle(PEP8), pychecker, pyflakes, flake8, mypy(PEP 484)                   # pylint ../soccerpy/proceesCVS.py 
# c. nose2 -v, pytest                                                                        # test runners are: unittest/nose or nose2/pytest
# d. tox                                                                                     # pip install tox, automates testing in multiple environments.
# e. bandit ../soccerpy/proceesCVS.py                                                        # checking for common security mistakes or vulnerabilities                                                 
# Sonarqube
# a. Install sonarqube server and python plugin
# b. Install sonar scanner, edit sonar-scanner.properties and sonar-project.properties       # $SONAR_SCANNER_PATH/conf/sonar-scanner.properties
# c. Sonar-scanner(define pylint first), open project in sonarqube server                    # sonar-scanner will show result of pylint, and code-coverage
####################################
# test_processCSV.py
import unittest, mock
import pandas as pd
from pandas.util.testing import assert_frame_equal
import soccerpy

class TestMyTestCase(unittest.TestCase):	
          # we test with mock first
          @mock.patch('soccerpy.processCVS.main_process') 
          def test_proceeCVSFile_withmock(self, mock_main_process):
                  # you can get this when developing soccerPy code
                  # Team,Games,Wins,Losses,Draws,Goals,Goals Allowed,Points
                  # Derby,38,8,6,24,33,63,30
                  df_Most-Draws_Expected=pd.DataFrame({'Team': [Derby], 'Games': [38], 'Wins': [8], 'Losses': [6],  
                                                       'Draws': [24], 'Goals': [33], 'Goals Allowed': [63], 'Points': [30]})
                  mock_main_process.return_value = df_Most-Draws_Expected
                  df_Most-Draws_Result=soccerpy.processCVS.main()
                  mock_main_process.assert_called_once_with()
                  assert_frame_equal(df_Most-Draws_Result, df_Most-Draws_Expected)

          # we test real code second
          def test_proceeCVSFile(self):
                  # you can get this when developing soccerPy code
                  df_Most-Draws_Expected=pd.DataFrame({'Team': [Derby], 'Games': [38], 'Wins': [8], 'Losses': [6],  
                                                       'Draws': [24], 'Goals': [33], 'Goals Allowed': [63], 'Points': [30]})
                  df_Most-Draws_Result=soccerpy.processCVS.main()
                  assert_frame_equal(df_Most-Draws_Result, df_Most-Draws_Expected)

    	
