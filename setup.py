# setup.py
from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(name='soccerpy',
      version = "1.0.0",
      author = "hangxia qiu",
      author_email = "hangxiaq@yahoo.com",
      scripts=['bin/run_proceesCVS', 'bin/run_xmldiff'],
      description="Data Processing Pakcage",
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      zip_safe=False,
      )
