Soccer code exercise instructions
===

The `soccer.csv` file contains the results from the English Premier League.

Write a program to read the file, then print the following stats:

- Show the team with the smallest difference in 'for' and 'against' goals.
- List the top 10 teams with the highest win percentage.
- Full stats for team with the most draws (include all columns available in CSV file)

All output should be directed to STDOUT and instructions on how to run the program should be included in a README file.

# instruction
--------------------------------------------------------------------------------------------------------------
1. Setup git and python3 in ubuntu(My and Your work)
   assume name="Ubuntu", Version="18.04.5 LTS(Bionic Beaver)" as linux         
   user is hxq29 and have sudo permission, default home directory is /home/hxq29/                                      
   assume Python 3.6.9 is installed in ubuntu as system python                                                            # otherwise install python3 python3-develop
   sudo apt-get install python3-distutils, curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py                        # get latest pip setuptools wheel
   python get-pip.py                                                                                                      # installed in /home/hxq29/.local/bin
   sudo vi /etc/environment, pip --version: pip 21.1.2,                                                                   # add /home/hxq29/.local/bin(for pip) to PATH
   Git: sudo apt install git, git --version                                                                               # git version git version 2.17.1.
        git config --global user.name "hxq29", git config --global user.email "hangxiaq@yahoo.com"
        gitlab: Add public key to settings SSH keys of member hxq29                                                       # use hxq29
        HTTPS:  $ git clone -b main https://gitlab.com/HQ/soccer.git                                                      # for https protocol
        SSH:    $ git clone -b main git@gitlab.com/HQ/soccer.git                                                          # equivalent to ssh://git@ ssh protocol
                $ cd soccer, git add --all, git commit -m "a message", git push, git status, git branch -vv, git branch -av, git remote -v, git remote show origin, git log 
---------------------------------

2. Develop/Test soccer project, Build a Wheel package and Push into PyPI(My work)
   A. Develop/Test soccer project
      sudo apt-get install libxml2-dev libxslt-dev python-dev                                                             # install system package for xml and xslt
      # develop and test soccer project code and push into gitlab                                                         # process soccer code is developed and pushed into gitlab from /home/hxq29/Soccer 
      git clone -b main https://gitlab.com/HQ/soccer.git                                                                  # get code from my personal gitlab account
      cd soccer, pip install -r requirements.txt, python soccerpy/proceesCVS.py, pylint soccerpy/proceesCVS.py            # develop/test/qa soccer project in /home/hxq29/soccer                                                                              
      python setup.py sdist bdist_wheel, python setup.py develop, nose2 -v                                                # run all test in tests folder
      git add --all, git commit -m "final Soccer project, add README.txt", git push                                       # push into gitlab, once finished

   B. Built soccerpy Wheel package and Push into PyPI(not yet)    
      .pypirc                                                                                                             # create file .pypirc with following content in /home/hxq29/                      
      [pypirc]
      servers = pypi
      [server-login]
      username:your_awesome_username
      password:your_awesome_password

      python setup.py register                                                                                            # This step only reserves the name of the package(soccerpy)
      python setup.py sdist bdist_wheel                                                                                   # build python wheel package
      twine upload dist/*                                                                                                 # upload package(soccerpy) into PyPI

   C. CICD pipeline in jenkins (need improve)
      Create a Jenkins pipeline job                                                                                       # jekins listen to web post message from gitlab
      start jenkins CICD pipeline for test and sonar-scanner                                                              # make soue sonarqube server is stared, and sonar-sanner is configured
---------------------------------

3. install soccerpy package in your linux and run csv processing code(Your work)
   A. install soccerpy package with setup.py                                                                                        
      cd, git clone -b main https://gitlab.com/HQ/soccer.git, cd soccer                                                   # distribution root(source distribution), directory where setup.py exists                                                                                     
      sudo python setup.py install                                                                                        # build and install into /usr/local/lib/python3.6.9/dist-packages/soccerpy. 
      # pip install soccerpy                                                                                              # setuptools will copy the run_csvprocess and run_xmldiff to $PATH 
                                                                                                                          # you can install from PyPI, after I upload package(soccerpy) into PyPI     
   B. process soccer.csv
      $ cd, pip install panda                                                                
      $ run_csvprocess --file /home/hxq29/soccer.csv                                                                      # soccer.csv should be in /home/hxq29/, you can see result from STDOUT

   C. compare two xml difference                                                                                          # I incluse python code to compare two xml files
      sudo apt-get install libxml2-dev libxslt-dev python-dev                                                             # install system package for xml and xslt
      sudo pip install lxml                                                                                               # install python xml parser
      run_xmldiff colordiff /home/hxq29/aABC1.xml /home/hxq29/aABC2.xml                                                   # aABC1.xml and aABC2.xml should be in /home/hxq29/ 
--------------------------------------------------------------------------------------------------------------